## spark k8s jupyterhub, anaconda environment
- Spark in k8s on jupyterhub materials
- Blog post https://dev.to/akoshel/spark-on-k8s-in-jupyterhub-1da2
- JupyterHub Architecture:
https://docs.google.com/document/d/1L6eH2o1KotN0xlRoLUJ3_1xhAGbghCslzG0UYEK-1zY/edit
---
## Setup Spark
#### Step 1: create 2 namespace spark and jupyterhub
```
kubectl apply -f spark_ns.yaml
kubectl apply -f jupyterhub_ns.yaml 
```
#### Step 2: Create service account spark in spark namespace
```
kubectl apply -f spark_sa.yaml
kubectl apply -f spark_sa_role.yaml
```
Test Spark submit
```
spark-submit \
  --master k8s://https://192.168.49.2:8443 \
  --deploy-mode cluster \
  --driver-memory 1g \
  --conf spark.kubernetes.memoryOverheadFactor=0.5 \
  --name sparkpi-test1 \
  --class org.apache.spark.examples.SparkPi \
  --conf spark.kubernetes.container.image=ducdn01/spark:latest \
  --conf spark.kubernetes.driver.pod.name=spark-test1-pi \
  --conf spark.kubernetes.namespace=spark \
  --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark \
  --verbose \
  local:///opt/spark/examples/jars/spark-examples_2.12-3.2.2.jar 1000
```
---
## Setup Jupyter Hub
Create service account
```
kubectl apply -f jupyterhub_sa.yaml
kubectl apply -f jupyterhub_sa_role.yaml
```
- We can use Rolebinding in Spark namespace for SA in JupyterHub. By adding SA to the subjects field of Rolebinding. You can see `jupyterhub_sa_role.yaml`

Create service cluster ip in jupyterhub namespace
```
kubectl apply -f driver_service.yaml
```
Add helm chart of JupyterHub
```
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
```
Setup JupyterHub Helm Chart
```
helm upgrade --cleanup-on-fail \
  --install <helm-release-name> jupyterhub/jupyterhub \
  --namespace <k8s-namespace> \
  --create-namespace \
  --version=<chart-version> \
  --values config.yaml
```
```
helm upgrade --cleanup-on-fail \
--install jupyterhub jupyterhub/jupyterhub \
--namespace jupyterhub \
--create-namespace \
--version=2.0.0 \
--values jhub_values.yaml
```
- Install the chart configured by your config.yaml, We can customize config.yaml
- Template config.yaml is installed and saved in file `values.yaml`
- My customizing file is modified in `jhub_values.yaml`
### Customize config.yaml
We can modify config of hub, single user,... You can see template in `values.yaml`
- Example customize single user
  - When creating a user account, will create a corresponding user pod of that user, and this pod will pull the image that we passed in the config file
  - Therefore, we can pass any image (You should customize your image from jupyter image such as [jupyter/datascience-notebook](https://hub.docker.com/r/jupyter/datascience-notebook/))
  - In here, I used my image `ducdn01/singleuser`, dockerfile is in above.
  - Each user will use a persisten volume claim to store data, and we can customize capactiy in `single user` component
![Alt text](image-1.png)
  - You can reference https://z2jh.jupyter.org/en/stable/jupyterhub/customizing/user-storage.html
```
singleuser:
  cpu:
    limit: 1
    guarantee: 0.5
  memory:
    limit: 2G
    guarantee: 1G
  defaultUrl: "/lab"
  serviceAccountName: spark
  image:
    name: ducdn01/singleuser
    tag: v7
    pullPolicy: IfNotPresent
    pullSecrets: []
```
- Example customize hub
  - Authen and author user is configed in  `hub component`
  - There are some ways to authen and author `Dummy Authenticator` `Github` `Google` `Azure`, ... You can reference https://z2jh.jupyter.org/en/stable/administrator/authentication.html
  - For testing purpose, I use Dummy Authenticator (default)
```
hub:
  config:
    GitHubOAuthenticator:
      allowed_organizations:
        - my-github-organization
      scope:
        - read:org
```
---
### Testing with code Pyspark
Spark Test
- Install pyspark
```
pip install pyspark==3.2.2
```

```
from pyspark.sql import SparkSession

# create spark session
spark = (
    SparkSession.builder.appName("hello").master("k8s://https://192.168.49.2:8443") # Your master address name
    .config("spark.kubernetes.container.image", "ducdn01/pyspark:latest") # Spark image name
    .config("spark.driver.port", "2222") # Needs to match svc
    .config("spark.driver.blockManager.port", "7777")
    .config("spark.driver.host", "driver-service.jupyterhub.svc.cluster.local") # Needs to match svc
    .config("spark.driver.bindAddress", "0.0.0.0")
    .config("spark.kubernetes.namespace", "spark")
    .config("spark.kubernetes.authenticate.driver.serviceAccountName", "spark")
    .config("spark.kubernetes.authenticate.serviceAccountName", "spark")

    .config("spark.executor.instances", "2") # Specified the number of excutor
    .config("spark.kubernetes.container.image.pullPolicy", "IfNotPresent")
    .getOrCreate()
)

simpleData = [("James","Sales","NY",90000,34,10000),
    ("Michael","Sales","NY",86000,56,20000),
    ("Robert","Sales","CA",81000,30,23000),
    ("Maria","Finance","CA",90000,24,23000),
    ("Raman","Finance","CA",99000,40,24000),
    ("Scott","Finance","NY",83000,36,19000),
    ("Jen","Finance","NY",79000,53,15000),
    ("Jeff","Marketing","CA",80000,25,18000),
    ("Kumar","Marketing","NY",91000,50,21000)
  ]

schema = ["employee_name","department","state","salary","age","bonus"]
df = spark.createDataFrame(data=simpleData, schema = schema)
df.printSchema()
df.show(truncate=False)

df.groupBy("department").sum("salary").show(truncate=False)
```
- Note: Stop spark session (pod worker is deleted)
```
spark.stop()
```
---
### Actice anaconda eviroment
- Create enviroment anaconda
```
conda create --name <env_name>
```
- Activate env
```
source activate <env_name>
```
- Install ipykernel
```
conda install ipykernel
```
- Add kernel in JupyterHub
```
python -m ipykernel install --user --name <enviroment name> –display-name "your name eviroment"
```
- Check anaconda in JupyterHub
```
import conda

anaconda_version = conda.__version__
print("Phiên bản Anaconda đang được sử dụng là:", anaconda_version)
```
- References: https://towardsdatascience.com/get-your-conda-environment-to-show-in-jupyter-notebooks-the-easy-way-17010b76e874
---
#### Uninstall kernel not uninstall env anaconda
```
jupyter kernelspec uninstall new-env
```
